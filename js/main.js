$(document).ready(function(){
    // Create a new WebSocket object.
    websocket = new WebSocket('ws://chat.loc:12345/server/server.php');

    // Connection is open.
    websocket.onopen = function(ev) {
        set_system_status('Connected!');
    }

    // Message received from server.
    websocket.onmessage = function(ev) {
        // PHP sends Json data.
        msg = JSON.parse(ev.data);

        if (msg.type == 'usermsg') {
            print_chat_message(msg.name, msg.message);
        }
        if (msg.type == 'system') {
            print_chat_message('System message!', msg.message);
        }
        // Reset text.
        $('.message-input').val('');
    };

    websocket.onerror = function(ev) {
        print_chat_message('System message!', ev.data, 'error');
    };

    websocket.onclose = function(ev) {
        set_system_status('Connection Closed!', false);
    };

    // Send message.
    $('.send-message').click(function() {
        // Get user message.
        message = $('.message-input').val();
        // Get user name.
        name = $('.name-input').val();

        if (name == '')  {
            alert('Enter your Name please!');
            return;
        }
        if (message == '') {
            alert('Enter Some message Please!');
            return;
        }
        // Hide name input.
        $('.name-input').addClass('hidden');

        // Convert and send data to server.
        websocket.send(JSON.stringify({
            message: message,
            name: name,
        }));
    });

    // Send message on press 'enter'.
    $('.message-input').on('keyup', function(e) {
        if (e.keyCode == 13) {
            $('.send-message').trigger('click');
        }
    });
});

// Set chat status.
function set_system_status(message, success = true) {
    if (success === true) {
        $('.system-message').removeClass('error').addClass('success').text(message);
    }
    else {
        $('.system-message').removeClass('success').addClass('error').text(message);
    }
}

// Print chat message.
function print_chat_message(name, message, m_class) {
    $('#messages').append('<div class="row message-bubble">\
        <div class="text-muted">' + name + '</div>\
        <div class="' + m_class + '">' + message + '</div>\
    </div>');
    $('.chat-body').scrollTop(1E10);
}